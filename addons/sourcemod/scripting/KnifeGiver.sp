#pragma semicolon 1

#include <cstrike>
#include <sdktools>
#include <clientprefs>
#include <multicolors>

#pragma newdecls required

#define PLUGIN_VERSION "1.0"

Handle g_hLastUsed = INVALID_HANDLE;
int g_iLastUsed[MAXPLAYERS+1] = { -1, ... };
bool g_bLate = false;

public Plugin myinfo = 
{
    name = "Knife Giver",
    author = ".Rushaway",
    description = "Give a knife on player request.",
    version = PLUGIN_VERSION,
    url = ""
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	g_bLate = late;
	return APLRes_Success;
}

public void OnPluginStart()
{
	RegConsoleCmd("sm_knife", Command_GiveKnife, "Give a knife to player.");

	g_hLastUsed = RegClientCookie("knifer_giver", "Last knife given", CookieAccess_Protected);
	
	HookEvent("player_death", Event_PlayerDeath);
	HookEvent("round_end", EventRoundEnd, EventHookMode_Pre);

	// Late load
	if (g_bLate)
	{
		for (int i = 1; i <= MaxClients; i++)
		{
			if (IsClientConnected(i))
				OnClientPutInServer(i);
		}
	}
}
public void OnClientPutInServer(int client)
{
	if (AreClientCookiesCached(client))
		ReadClientCookies(client);
}

public void OnClientDisconnect(int client)
{
	g_iLastUsed[client] = 0;
	SetClientCookies(client);
}

public void OnClientCookiesCached(int client)
{
	ReadClientCookies(client);
}

public void ReadClientCookies(int client)
{
	char sValue[32];

	GetClientCookie(client, g_hLastUsed, sValue, sizeof(sValue));
	g_iLastUsed[client] = (sValue[0] == '\0' ? 0 : StringToInt(sValue));
}

public void SetClientCookies(int client)
{
	char sValue[32];

	Format(sValue, sizeof(sValue), "%i", g_iLastUsed[client]);
	SetClientCookie(client, g_hLastUsed, sValue);
}

public Action EventRoundEnd(Handle hEvent, char[] chEvent, bool bDontBroadcast)
{
	for(int client = 0; client < MAXPLAYERS; client++)
		g_iLastUsed[client] = 0;		
}

public void Event_PlayerDeath(Handle event, const char[] name, bool dontBroadcast)
{
    int client = GetClientOfUserId(GetEventInt(event, "userid"));
    g_iLastUsed[client] = 0;
}

public Action Command_GiveKnife(int client, int args)
{
	if (client == 0)
	{
		ReplyToCommand(client, "[SM] Can't use this command from server console.");
		return Plugin_Handled;
	}
	if (!IsPlayerAlive(client))
	{
		CReplyToCommand(client, "{green}[SM] {default}You need to be alive to use this command.");
		return Plugin_Handled;
	}
	
	if (g_iLastUsed[client] != 0)
	{
		CReplyToCommand(client, "{green}[SM] {default}You can get a knife only {red}ONE time per life{default}.");
		return Plugin_Handled;
	}

	if(IsClientInGame(client) && IsPlayerAlive(client))
	{
		g_iLastUsed[client] = 1;
		GivePlayerItem(client, "weapon_knife");
		CReplyToCommand(client, "{green}[SM] {default}Here you go. You got a fresh knife.");
		CReplyToCommand(client, "{green}[SM] {default}You can get only one per life.");
	}
	return Plugin_Handled;
}
